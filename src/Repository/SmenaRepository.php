<?php

namespace App\Repository;

use App\Entity\Smena;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Smena|null find($id, $lockMode = null, $lockVersion = null)
 * @method Smena|null findOneBy(array $criteria, array $orderBy = null)
 * @method Smena[]    findAll()
 * @method Smena[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmenaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Smena::class);
    }

    // /**
    //  * @return Smena[] Returns an array of Smena objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Smena
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
