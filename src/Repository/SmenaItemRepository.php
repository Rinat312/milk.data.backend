<?php

namespace App\Repository;

use App\Entity\SmenaItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SmenaItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmenaItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmenaItem[]    findAll()
 * @method SmenaItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmenaItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SmenaItem::class);
    }

    // /**
    //  * @return SmenaItem[] Returns an array of SmenaItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SmenaItem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
