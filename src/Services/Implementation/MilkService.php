<?php

use App\Entity\Client;
use App\Entity\Smena;
use App\Entity\SmenaItem;

class MilkService implements IMilkService{

    public function create(MilkView $milkView){
        $em = $this->GetDoctrine()->getManager();
        $client = new Client();
        $smena = new Smena();
        $smenaItem = new SmenaItem();

        foreach ($milkView->clientArray as $clientItem){
            $client->setClientId($clientItem->client_id);
            $client->setName($clientItem->name);
            $client->setStatus($clientItem->status);
            $client->setUserId($clientItem->user_id);
            $client->setPosition($clientItem->position);

            $em->merge($client);
        }

        foreach ($milkView->smenaArray as $smenaDataItem){
            $smena->setSmenaId($smenaDataItem->smena_id);
            $smena->setName($smenaDataItem->name);
            $date = new \DateTime($smenaDataItem->create_date);
            $smena->setCreateDate($date);

            $em->merge($smena);
        }

        foreach ($milkView->smenaItemArray as $smenaClientDataItem){
            $smenaItem->setSmena($smenaClientDataItem->smena_id);
            $smenaItem->setClient($smenaClientDataItem->client_id);
            $smenaItem->setAmount($smenaClientDataItem->amount);

            $em->merge($smenaItem);
        }

        $em->flush();
    }
}