<?php

namespace App\Controller;

use App\Entity\Smena;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Serializer\Serializer;
use FOS\RestBundle\View\View;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/")
     * @Rest\View()
     */
    public function index()
    {
        $smena = new Smena();
        $smena->setId(1);
        $smena->setName('Name');
        $view = $this->view($smena, 200);
        return $this->handleView($view);
    }
}
