<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Smena;
use App\Entity\SmenaItem;
use App\Model\MilkView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Karriere\JsonDecoder\Bindings\ArrayBinding;
use Karriere\JsonDecoder\JsonDecoder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MilkController
 * @package App\Controller
 * @Rest\Route("/milk")
 */
class MilkController extends FOSRestController
{
//    private $milkService;
//    public function __construct(\IMilkService $IMilkService)
//    {
//        $this->milkService = $IMilkService;
//    }

    /**
     * @Rest\Post("/add")
     * @Rest\View()
     */
    public function addAction(Request $request){

       $jsonDecoder = new JsonDecoder();
       $milkView = new MilkView();

       try{
           $clientRequest = $request->get("client");
           $smenaRequest = $request->get("smena");
           $smenaItemRequest = $request->get("smenaItem");

           $clientJson = json_encode($clientRequest);
           $smenaJson = json_encode($smenaRequest);
           $smenaItemJson = json_encode($smenaItemRequest);

           $clientArray = $jsonDecoder->decodeMultiple($clientJson, Client::class);
           $smenaArray = $jsonDecoder->decodeMultiple($smenaJson, Smena::class);
           $smenaItemArray= $jsonDecoder->decodeMultiple($smenaItemJson, SmenaItem::class);

           $milkView->clientArray = $clientArray;
           $milkView->smenaArray= $smenaArray;
           $milkView->smenaItemArray = $smenaItemArray;

           //$result = $this->milkService->create($milkView);
           $this->create($milkView);

           foreach ($milkView->smenaArray as $smenaDataItem){
               $date = new \DateTime($smenaDataItem->create_date);
               $smenaDataItem->create_date = $date;
           }

           $view = $this->view($milkView, 200);
           return $this->handleView($view);
       }
       catch (\Exception $ex){
           $view = $this->view($ex->getMessage(), 400);
           return $this->handleView($view);
       }
    }

    private function create(MilkView $milkView){
        $em = $this->GetDoctrine()->getManager();
        $client = new Client();
        $smena = new Smena();
        $smenaItem = new SmenaItem();

        foreach ($milkView->clientArray as $clientItem){

            $this->checkClient($clientItem->client_id);

            $client->setClientId($clientItem->client_id);
            $client->setName($clientItem->name);
            $client->setStatus($clientItem->status);
            $client->setUserId($clientItem->user_id);
            $client->setPosition($clientItem->position);

            $em->merge($client);
        }

        foreach ($milkView->smenaArray as $smenaDataItem){

            $this->checkSmena($smenaDataItem->smena_id);

            $smena->setSmenaId($smenaDataItem->smena_id);
            $smena->setName($smenaDataItem->name);
            $date = new \DateTime($smenaDataItem->create_date);
            $smena->setCreateDate($date);

            $em->merge($smena);
        }

        foreach ($milkView->smenaItemArray as $smenaClientDataItem){
            $smenaItem->setSmena($smenaClientDataItem->smena_id);
            $smenaItem->setClient($smenaClientDataItem->client_id);
            $smenaItem->setAmount($smenaClientDataItem->amount);

            $em->merge($smenaItem);
        }
        $em->flush();
    }

    private function checkClient($client_id){
        $existClient = $this->getDoctrine()
            ->getRepository(Client::class)
            ->findBy(array('client_id' => $client_id));

        if ($existClient != null) {
            throw $this->createNotFoundException(
                'Клиент с таким идентификатором уже существует: '.$client_id
            );
        }
    }

    private function checkSmena($smena_id){
        $existSmena = $this->getDoctrine()
            ->getRepository(Smena::class)
            ->findBy(array('smena_id' => $smena_id));

        if ($existSmena != null) {
            throw $this->createNotFoundException(
                'Смена с таким идентификатором уже существует: '.$smena_id
            );
        }
    }
}