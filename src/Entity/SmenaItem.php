<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SmenaItemRepository")
 */
class SmenaItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    public $client_id;

    /**
     * @ORM\Column(type="integer")
     */
    public $smena_id;

    /**
     * @ORM\Column(type="float")
     */
    public $amount;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client_id = $client;
    }

    /**
     * @return mixed
     */
    public function getSmena()
    {
        return $this->smena_id;
    }

    /**
     * @param mixed $smena
     */
    public function setSmena($smena)
    {
        $this->smena_id = $smena;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
